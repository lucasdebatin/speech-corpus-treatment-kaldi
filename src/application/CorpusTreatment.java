package application;

import application.constituicaobr.ConstituicaoBRHTK;
import application.constituicaobr.ConstituicaoBRKaldi;
import application.constituicaobr.ConstituicaoBRSphinx;
import application.constituicaobr.ConstituicaoBRLexicon;
import application.lapsbm.LapsBMHTK;
import application.lapsbm.LapsBMSphinx;
import application.lapsbm.LapsBMKaldi;
import application.lapsbm.LapsBMLexicon;

import java.io.*;

public class CorpusTreatment {
    private static String diretorioPrincipal = "src"+File.separator+"resources"+File.separator;

    public static void main(String[] args) throws IOException {
        LapsBM();
        ConstituicaoBR();
    }

    private static void ConstituicaoBR() throws IOException {
        //limpar
        File diretorioDeletarKaldi = new File(diretorioPrincipal + "constituicaobr-kaldi" + File.separator);
        for (File arquivo : diretorioDeletarKaldi.listFiles()) {
            arquivo.delete();
        }
        limparPastas(diretorioPrincipal + "constituicaobr-kaldi" + File.separator + "test" + File.separator);
        limparPastas(diretorioPrincipal + "constituicaobr-kaldi" + File.separator + "train" + File.separator);

        File diretorioDeletarSphinx = new File(diretorioPrincipal + "constituicaobr-sphinx" + File.separator);
        for (File arquivo : diretorioDeletarSphinx.listFiles()) {
            arquivo.delete();
        }
        limparPastas(diretorioPrincipal + "constituicaobr-sphinx" + File.separator + "test" + File.separator);
        limparPastas(diretorioPrincipal + "constituicaobr-sphinx" + File.separator + "train" + File.separator);

        File diretorioDeletarHTK = new File(diretorioPrincipal + "constituicaobr-htk" + File.separator);
        for (File arquivo : diretorioDeletarHTK.listFiles()) {
            arquivo.delete();
        }
        limparPastas(diretorioPrincipal + "constituicaobr-htk" + File.separator + "test" + File.separator);
        limparPastas(diretorioPrincipal + "constituicaobr-htk" + File.separator + "train" + File.separator);

        limparPastas(diretorioPrincipal + "constituicaobr-lexicon" + File.separator);
        //fim limpar

        ConstituicaoBRKaldi constituicaoBRKaldi = new ConstituicaoBRKaldi();
        constituicaoBRKaldi.CorpusTreatment();
        System.out.println("ConstituicaoBR Kaldi finalizado!");

        ConstituicaoBRSphinx constituicaoBRSphinx = new ConstituicaoBRSphinx();
        constituicaoBRSphinx.CorpusTreatment();
        System.out.println("ConstituicaoBR Sphinx finalizado!");

        ConstituicaoBRHTK constituicaoBRHTK = new ConstituicaoBRHTK();
        constituicaoBRHTK.CorpusTreatment();
        System.out.println("ConstituicaoBR HTK finalizado!");

        ConstituicaoBRLexicon lexicon = new ConstituicaoBRLexicon();
        lexicon.lexiconTreatmentCaracteres();
        System.out.println("ConstituicaoBR Lexicon Caracteres finalizado!");

        lexicon.lexiconPhones();
        System.out.println("ConstituicaoBR Lexicon Phones finalizado!");

        lexicon.HTK();
        System.out.println("ConstituicaoBR Lexicon HTK finalizado!");
    }

    private static void LapsBM() throws IOException {
        //limpar
        File diretorioDeletarKaldi = new File(diretorioPrincipal + "lapsbm-kaldi" + File.separator);
        for (File arquivo : diretorioDeletarKaldi.listFiles()) {
            arquivo.delete();
        }
        limparPastas(diretorioPrincipal + "lapsbm-kaldi" + File.separator + "test" + File.separator);
        limparPastas(diretorioPrincipal + "lapsbm-kaldi" + File.separator + "train" + File.separator);

        File diretorioDeletarSphinx = new File(diretorioPrincipal + "lapsbm-sphinx" + File.separator);
        for (File arquivo : diretorioDeletarSphinx.listFiles()) {
            arquivo.delete();
        }
        limparPastas(diretorioPrincipal + "lapsbm-sphinx" + File.separator + "test" + File.separator);
        limparPastas(diretorioPrincipal + "lapsbm-sphinx" + File.separator + "train" + File.separator);

        File diretorioDeletarHTK = new File(diretorioPrincipal + "lapsbm-htk" + File.separator);
        for (File arquivo : diretorioDeletarHTK.listFiles()) {
            arquivo.delete();
        }
        limparPastas(diretorioPrincipal + "lapsbm-htk" + File.separator + "test" + File.separator);
        limparPastas(diretorioPrincipal + "lapsbm-htk" + File.separator + "train" + File.separator);

        limparPastas(diretorioPrincipal + "lapsbm-lexicon" + File.separator);
        //fim limpar

        LapsBMKaldi lapsBMKaldi = new LapsBMKaldi();
        lapsBMKaldi.CorpusTreatment();
        System.out.println("LapsBM Kaldi finalizado!");

        LapsBMSphinx lapsBMSphinx = new LapsBMSphinx();
        lapsBMSphinx.CorpusTreatment();
        System.out.println("LapsBM Sphinx finalizado!");

        LapsBMHTK lapsBMHTK = new LapsBMHTK();
        lapsBMHTK.CorpusTreatment();
        System.out.println("LapsBM HTK finalizado!");

        LapsBMLexicon lexicon = new LapsBMLexicon();
        lexicon.lexiconTreatmentCaracteres();
        System.out.println("LapsBM Lexicon Caracteres finalizado!");

        lexicon.lexiconPhones();
        System.out.println("LapsBM Lexicon Phones finalizado!");

        lexicon.HTK();
        System.out.println("LapsBM Lexicon HTK finalizado!");
    }

    private static void limparPastas(String pasta) {
        File diretorioDeletar = new File(pasta);
        if (diretorioDeletar.exists()) {
            for (File arquivo : diretorioDeletar.listFiles()) {
                if (arquivo.isFile()) {
                    arquivo.delete();
                } else {
                    for (File audio : arquivo.listFiles()) {
                        audio.delete();
                    }
                    arquivo.delete();
                }
            }
            diretorioDeletar.delete();
        }
    }
}