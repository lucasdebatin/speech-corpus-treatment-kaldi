package application.constituicaobr;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class ConstituicaoBRKaldi {
    private static String diretorioPrincipal = "src"+ File.separator+"resources"+File.separator;
    private static String pastasPrincipal[] = {"train", "test"};
    private static HashSet<String> spk2gender = new HashSet();
    private static HashSet<String> wavscp = new HashSet();
    private static HashSet<String> text = new HashSet();
    private static HashSet<String> utt2spk = new HashSet();
    private static HashSet<String> corpus = new HashSet();
    private static String locutor = "ConstituicaoM001";

    public void CorpusTreatment() throws IOException {
        for (String pasta : pastasPrincipal) {
            spk2gender.clear();
            wavscp.clear();
            text.clear();
            utt2spk.clear();
            File arquivos = new File(diretorioPrincipal + "ConstituicaoBR16KHz" + File.separator + pasta + File.separator);
            spk2gender.add(locutor + " m");
            for (File arquivo : arquivos.listFiles()) {
                if (arquivo.getName().endsWith(".wav")) {
                    String caminhoSplit[] = arquivo.getPath().split("/");
                    String nome = locutor + "_" + caminhoSplit[4].replace(".wav", "");
                    String caminho = "/home/debatin/Documents/Toolkits/kaldi-trunk/egs/ocsr-cbr/data/" + caminhoSplit[3] + File.separator + locutor + File.separator + caminhoSplit[4];
                    wavscp.add(nome + " " + caminho);
                    transferirArquivo(arquivo);
                } else if(arquivo.getName().endsWith(".txt")) {
                    String caminhoSplit[] = arquivo.getPath().split("/");
                    String nome = locutor + "_" + caminhoSplit[4].replace(".txt", "");
                    BufferedReader lerArquivo = new BufferedReader(new FileReader(arquivo));
                    String texto = acordoOrtografico(lerArquivo.readLine()).trim();
                    text.add(nome + " " + texto);
                    utt2spk.add(nome + " " + locutor);
                    corpus.add(texto);
                }
            }
            gravarArquivos(pasta);
        }
        gravaArquivo("corpus.txt", corpus);
    }

    private void transferirArquivo(File arquivo) throws IOException {
        String caminhoSplit[] = arquivo.getPath().split("/");
        File diretorioDestino = new File(diretorioPrincipal + "constituicaobr-kaldi" + File.separator + caminhoSplit[3] + File.separator + locutor + File.separator);
        diretorioDestino.mkdirs();
        File arquivoDestino = new File (diretorioDestino.getPath() + File.separator + caminhoSplit[4]);
        InputStream in = new FileInputStream(arquivo);
        OutputStream out = new FileOutputStream(arquivoDestino);
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    private String acordoOrtografico(String linha) {
        linha = linha.replace("   ", " ");
        linha = linha.replace("  ", " ");
        linha = linha.replace("ü", "u");
        linha = linha.replace("assembléia", "assembleia");
        linha = linha.replace("idéia", "ideia");
        return linha;
    }

    private void gravarArquivos(String pasta) throws IOException {
        gravaArquivo(pasta+File.separator+"spk2gender", spk2gender);
        gravaArquivo(pasta+File.separator+"wav.scp", wavscp);
        gravaArquivo(pasta+File.separator+"text", text);
        gravaArquivo(pasta+File.separator+"utt2spk", utt2spk);
    }

    private void gravaArquivo(String arquivo, HashSet<String> hs) throws IOException {
        List<String> sortedList = new ArrayList(hs);
        if (arquivo != "corpus.txt") {
            Collections.sort(sortedList);
        }
        FileWriter gerarArquivo = new FileWriter(diretorioPrincipal + "constituicaobr-kaldi" + File.separator + arquivo, true);
        int aux = 0;
        String conteudo = "";
        for (String linha : sortedList) {
            if (aux != 0) {
                conteudo += "\n";
            } else {
                aux = 1;
            }
            conteudo += linha;
        }
        conteudo += "\n";
        gerarArquivo.write(conteudo);
        gerarArquivo.close();
        System.out.println("Arquivo '" + arquivo + "' gravado");
    }
}
