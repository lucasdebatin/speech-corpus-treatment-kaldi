package application.constituicaobr;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class ConstituicaoBRSphinx {
    private static String diretorioPrincipal = "src"+ File.separator+"resources"+File.separator;
    private static String pastasPrincipal[] = {"train", "test"};
    private static HashSet<String> testfileids = new HashSet();
    private static HashSet<String> trainfileids = new HashSet();
    private static HashSet<String> testtranscription = new HashSet();
    private static HashSet<String> traintranscription = new HashSet();
    private static String locutor = "ConstituicaoM001";

    public void CorpusTreatment() throws IOException {
        for (String pasta : pastasPrincipal) {
            File arquivos = new File(diretorioPrincipal + "ConstituicaoBR16KHz" + File.separator + pasta + File.separator);
            for (File arquivo : arquivos.listFiles()) {
                String caminhoSplit[] = arquivo.getPath().split("/");
                if (arquivo.getName().endsWith(".wav")) {
                    if (pasta.equals("train")) {
                        trainfileids.add(locutor + File.separator + caminhoSplit[4].replace(".wav", ""));
                    } else {
                        testfileids.add(locutor + File.separator + caminhoSplit[4].replace(".wav", ""));
                    }
                    transferirArquivo(arquivo);
                } else if(arquivo.getName().endsWith(".txt")) {
                    BufferedReader lerArquivo = new BufferedReader(new FileReader(arquivo));
                    String texto = acordoOrtografico(lerArquivo.readLine()).trim();
                    texto = locutor + File.separator + caminhoSplit[4].replace(".wav", "") + "<s> " + texto + " </s> (" + caminhoSplit[4].replace(".txt", "") + ")";
                    if (pasta.equals("train")) {
                        traintranscription.add(texto);
                    } else {
                        testtranscription.add(texto);
                    }
                }
            }
        }
        gravarArquivos();
    }

    private void transferirArquivo(File arquivo) throws IOException {
        String caminhoSplit[] = arquivo.getPath().split("/");
        File diretorioDestino = new File(diretorioPrincipal + "constituicaobr-sphinx" + File.separator + "wav" + File.separator + locutor + File.separator);
        diretorioDestino.mkdirs();
        File arquivoDestino = new File (diretorioDestino.getPath() + File.separator + caminhoSplit[4]);
        InputStream in = new FileInputStream(arquivo);
        OutputStream out = new FileOutputStream(arquivoDestino);
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    private String acordoOrtografico(String linha) {
        linha = linha.replace("   ", " ");
        linha = linha.replace("  ", " ");
        linha = linha.replace("ü", "u");
        linha = linha.replace("assembléia", "assembleia");
        linha = linha.replace("idéia", "ideia");
        return linha;
    }

    private void gravarArquivos() throws IOException {
        gravaArquivo("ocsr_train.fileids", trainfileids);
        gravaArquivo("ocsr_train.transcription", traintranscription);
        gravaArquivo("ocsr_test.fileids", testfileids);
        gravaArquivo("ocsr_test.transcription", testtranscription);
    }

    private void gravaArquivo(String arquivo, HashSet<String> hs) throws IOException {
        List<String> sortedList = new ArrayList(hs);
        Collections.sort(sortedList);
        FileWriter gerarArquivo = new FileWriter(diretorioPrincipal + "constituicaobr-sphinx" + File.separator + arquivo, true);
        int aux = 0;
        String conteudo = "";
        for (String linha : sortedList) {
            if (aux != 0) {
                conteudo += "\n";
            } else {
                aux = 1;
            }
            if (arquivo.equals("ocsr_train.transcription") || arquivo.equals("ocsr_test.transcription")) {
                linha = linha.substring(linha.split("<")[0].length());
            }
            conteudo += linha;
        }
        conteudo += "\n";
        gerarArquivo.write(conteudo);
        gerarArquivo.close();
        System.out.println("Arquivo '" + arquivo + "' gravado");
    }
}
