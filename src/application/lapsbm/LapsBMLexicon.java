package application.lapsbm;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class LapsBMLexicon {
    private static String diretorioPrincipal = "src"+File.separator+"resources"+File.separator;

    public void lexiconTreatmentCaracteres() throws IOException {
        HashSet<String> hs = new HashSet();
        File arquivo = new File(diretorioPrincipal + "LapsBMLexicon"+File.separator+ "lexicon.txt");
        BufferedReader lerArquivo = new BufferedReader(new FileReader(arquivo));
        String linha = lerArquivo.readLine();
        while (linha != null) {
            hs.add(alterarCaracteresPhones(linha));
            linha = lerArquivo.readLine();
        }
        gravaArquivo("lexicon.txt", hs);
    }

    public void lexiconPhones() throws IOException {
        HashSet<String> hs = new HashSet();
        File arquivo = new File(diretorioPrincipal +"lapsbm-lexicon"+File.separator+ "lexicon.txt");
        BufferedReader lerArquivo = new BufferedReader(new FileReader(arquivo));
        String linha = lerArquivo.readLine();
        while (linha != null) {
            String linhaSplit[] = linha.split(" ");
            for (int i = 1; i < linhaSplit.length; i++) {
                hs.add(linhaSplit[i]);
            }
            linha = lerArquivo.readLine();
        }
        gravaArquivo("phones.txt", hs);
    }

    public void HTK() throws IOException {
        trifoneTreatmentHTK();
        grammarHTK();
        lexiconTreatmentHTK();
    }

    private void grammarHTK() throws IOException {
        HashSet<String> hsGrammar = new HashSet();
        String conteudoGrammar = "$digit = ";
        int aux = 0;
        for (String word : words()) {
            if (aux != 0) {
                conteudoGrammar += " | ";
            } else {
                aux = 1;
            }
            conteudoGrammar += word;
        }
        conteudoGrammar += ";";
        hsGrammar.add(conteudoGrammar);
        hsGrammar.add("( sil < $digit sp > sil )");
        gravaArquivo("grammar_HTK", hsGrammar);

        HashSet<String> hsWordlist = new HashSet();
        for (String word : words()) {
            hsWordlist.add(word);
        }
        hsWordlist.add("<s>");
        hsWordlist.add("</s>");
        hsWordlist.add("!ENTER");
        hsWordlist.add("!EXiT");
        hsWordlist.add("!!NuLL");
        hsWordlist.add("silence");
        hsWordlist.add("sp");
        gravaArquivo("wordlist.txt_HTK", hsWordlist);
    }

    private void lexiconTreatmentHTK() throws IOException {
        HashSet<String> hs = new HashSet();
        File arquivo = new File(diretorioPrincipal +"lapsbm-lexicon"+File.separator+ "lexicon.txt");
        BufferedReader lerArquivo = new BufferedReader(new FileReader(arquivo));
        String linha = lerArquivo.readLine();
        while (linha != null) {
            String linhaSplit[] = linha.split(" ");
            String conteudo = linhaSplit[0] + "\t\t";
            for (int i = 1; i < linhaSplit.length; i++) {
                conteudo += linhaSplit[i] + " ";
            }
            hs.add(conteudo.trim() + " sp");
            linha = lerArquivo.readLine();
        }
        hs.add("</s>  []  sil");
        hs.add("<s>   []  sil ");
        hs.add("!ENTER  []  sil");
        hs.add("!EXIT   []     sil");
        hs.add("!EXiT   []     sil");
        hs.add("!!NULL   []   sil");
        hs.add("!!NuLL   []   sil");
        hs.add("!!UNK   []   sil");
        hs.add("silence\t\tsil");
        hs.add("sp\t\ts p sp");
        hs.add("sil             sil");
        gravaArquivo("dictionary.dic_HTK", hs);
    }

    private void trifoneTreatmentHTK() throws IOException {
        HashSet<String> hs = new HashSet();
        File arquivo = new File(diretorioPrincipal +"LapsBMLexicon"+File.separator+ "intWordTrifones_HTK.list");
        BufferedReader lerArquivo = new BufferedReader(new FileReader(arquivo));
        String linha = lerArquivo.readLine();
        while (linha != null) {
            if (!linha.contains("h")) {
                hs.add(alterarCaracteresPhones(linha));
            }
            linha = lerArquivo.readLine();
        }
        arquivo = new File(diretorioPrincipal +"LapsBMLexicon"+File.separator+ "trifone_HTK");
        lerArquivo = new BufferedReader(new FileReader(arquivo));
        linha = lerArquivo.readLine();
        while (linha != null) {
            hs.add(alterarCaracteresPhones(linha));
            linha = lerArquivo.readLine();
        }
        gravaArquivo("intWordTrifones.list_HTK", hs);
    }

    private void gravaArquivo(String arquivo, HashSet<String> hs) throws IOException {
        List<String> sortedList = new ArrayList(hs);
        if (!arquivo.equals("intWordTrifones.list_HTK")) {
            Collections.sort(sortedList);
        }
        File gerarPasta = new File(diretorioPrincipal +"lapsbm-lexicon"+File.separator+File.separator);
        gerarPasta.mkdir();
        FileWriter gerarArquivo = new FileWriter(gerarPasta + File.separator + arquivo, true);
        int aux = 0;
        String conteudo = "";
        for (String linha : sortedList) {
            if (aux != 0) {
                conteudo += "\n";
            } else {
                aux = 1;
            }
            conteudo += linha;
        }
        conteudo += "\n";
        gerarArquivo.write(conteudo);
        gerarArquivo.close();
        System.out.println("Arquivo '" + arquivo + "' gravado");
        hs.clear();
    }

    private HashSet<String> words() throws IOException {
        HashSet<String> hs = new HashSet();
        File arquivo = new File(diretorioPrincipal +"LapsBMLexicon"+File.separator+ "lexicon.txt");
        BufferedReader lerArquivo = new BufferedReader(new FileReader(arquivo));
        String linha = lerArquivo.readLine();
        while (linha != null) {
            String linhaSplit[] = linha.split(" ");
            hs.add(linhaSplit[0]);
            linha = lerArquivo.readLine();
        }
        return hs;
    }

    private String alterarCaracteresPhones(String texto) {
        texto = texto.replace("a~", "aa");
        texto = texto.replace("e~", "ee");
        texto = texto.replace("i~", "ii");
        texto = texto.replace("j~", "jj");
        texto = texto.replace("o~", "oo");
        texto = texto.replace("u~", "uu");
        texto = texto.replace("w~", "ww");
        texto = texto.replace("dZ", "dz");
        texto = texto.replace("tS", "ts");
        texto = texto.replace("E", "em");
        texto = texto.replace("J", "jm");
        texto = texto.replace("L", "lm");
        texto = texto.replace("O", "om");
        texto = texto.replace("R", "rm");
        texto = texto.replace("S", "sm");
        texto = texto.replace("X", "xm");
        texto = texto.replace("Z", "zm");

        return texto;
    }
}