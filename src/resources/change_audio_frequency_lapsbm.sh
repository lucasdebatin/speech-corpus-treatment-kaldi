#!/bin/bash
pathdir="/home/debatin/Desktop/LapsBM/"
rm -rf "/home/debatin/Desktop/LapsBM16KHz"
mkdir "/home/debatin/Desktop/LapsBM16KHz/"
cd "$pathdir"
for dir in *
do
    #echo "$dir"
    pathsubdir="/home/debatin/Desktop/LapsBM/$dir"
    mkdir "/home/debatin/Desktop/LapsBM16KHz/$dir/"
    cd "$pathsubdir"
    for subdir in *
    do
        #echo "$subdir"
        pathsubdir="/home/debatin/Desktop/LapsBM/$dir/$subdir"
        mkdir "/home/debatin/Desktop/LapsBM16KHz/$dir/$subdir/"
        cd "$pathsubdir"
        for filewav in $(ls -1 *.wav)
        do
            ffmpeg -i $filewav -acodec pcm_s16le -ar 16000 "/home/debatin/Desktop/LapsBM16KHz/$dir/$subdir/$filewav"
            #echo "$filewav"
        done
        for filetxt in $(ls -1 *.txt)
        do
            cp "$pathsubdir/$filetxt" "/home/debatin/Desktop/LapsBM16KHz/$dir/$subdir/$filetxt"
            #echo "$filetxt"
        done
    done
done
echo "Done!"
